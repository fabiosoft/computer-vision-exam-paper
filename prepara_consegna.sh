#!/bin/bash

echo "Lo script presuppone che alcuni file e cartelle siano già presenti"

#copia relazione pdf
cp "../_RELAZIONE/compile/Relazione_Elim_Nisci.pdf" "./Nisci_ELIM/relazione"
#copia sorgente latex
cp -R "../_RELAZIONE/compile/Relazione_Elim_Nisci_tex" "./Nisci_ELIM/relazione"
#copia file sorgente matlab aggiornato - esclude git
rsync -a --exclude='.git/' "../cvprcode" "./Nisci_ELIM/"

#zip del risultato
echo "Compressione..."
zip -r -X -q -9 "Nisci_ELIM.zip" "Nisci_ELIM"

#elimina file inutili alla consegna
rm "./Nisci_ELIM/relazione/res/pics/griglia_test.psd"
rm -r "./Nisci_ELIM/relazione/res/pics/griglia test/"
rm -r "./Nisci_ELIM/cvprcode/tmp_imgs/"
