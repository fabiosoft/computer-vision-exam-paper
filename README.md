# Rilevazione salienza attraverso classificazione multipla basata su grafo
#### Esame: Elaborazione delle Immagini
#### Autore: Fabio Nisci

### PanoramicaPredire dove gli occhi umani guardano quando osservano un’immagine ha guadagnato una popolarità significante negli ultimi anni. In questo scritto, sarà presentato un nuovo metodo per l’apprendimento bottom-up della salienza visiva, riferito alla parte dell’immagine che possiede maggiore importanza, la quale ben si adatta a individuare gli oggetti di interesse in scene complesse.
Nella presente relazione viene descritto e testato un algoritmo, sviluppato presso _Dalian University of Technology, OMRON Corporation e University of California at Merced._
Tempo di esecuzione per immagine 25.6 secondi.
Il tempo medio di esecuzione delle fasi attuali con migliori prestazioni, utilizzando l’implementazione _Matlab_ sul dataset **DUT-OMRON**, sono presentati nella Tabella 4.5 basati su macchina Macintosh: CPU Intel Core i5 2.7 GHz e 12GB RAM di metà 2011. Il tempo di analisi delle prestazioni di esecuzione è molto veloce: impiegando in media 25 secondi per ogni immagine. In particolare, la generazione dei superpixel dall’algoritmo SLIC spende 0.165 s (circa 64%), e l’effettivo calcolo della salienza spende 0,091 s.

|  Fase |  soglia media GT |   salienza singola soglia  |   totale (x52 soglie)  | 
|:-------:|:-----:|:-----:|:------:|
| Tempo(s) | 0,806 | 0,467 | 25,123 | 

### ConclusioniHo analizzato il proposto metodo bottom-up per rilevare le regioni salienti in immagini attraverso un grafo di classificazione, il quale incorpora raggruppamento locale e priorità dei margini. Adottato un approccio in due fasi con query di sfondo e primo piano per la classificazione e la generazione delle mappe di salienza. Valutato l’algoritmo su grandi dataset e dimostrato i risultati attesi con curve di precisione-richiamo. Inoltre, l’algoritmo proposto è **computazionalmente efficiente**.
---
	Fabio Nisci 2014. All rights reserved.	No part of this work may be reproduced in any form without 	prior permission of the author.	This work is for computer vision exam.	NOT for commercial use.