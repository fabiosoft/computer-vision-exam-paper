% Demo for paper "Saliency Detection via Graph-Based Manifold Ranking" 
% by Chuan Yang, Lihe Zhang, Huchuan Lu, Ming-Hsuan Yang, and Xiang Ruan
% To appear in Proceedings of IEEE Conference on Computer Vision and Pattern Recognition (CVPR 2013), Portland, June, 2013.
% Improved by Fabio Nisci, 2014

function [mapstage2, th] = saliency(imname,threshold)
    addpath('./others/');
%%------------------------ imposta parametri ---------------------%%
    theta=10; % controlla il peso degli archi
    alpha=0.99;% controlla il bilanciamento di due elementi
    spnumber=200;% numero di superpixel
    imgRoot='./test/';% path immagini di test
    saldir='./saliencymap/';% path di output della mappa di salienza
    supdir='./superpixels/';% path dei file di superpixel
   
    % se i percorsi non esistono, li crea
    if ~exist(supdir, 'dir')
        mkdir(supdir);
    end
    if ~exist(saldir, 'dir')
        mkdir(saldir);
    end

    impath=[imgRoot imname]; 
    [input_im,w]=removeframe(impath);% pre processo per rimuove la cornice dall'immagine
    [m,n,k] = size(input_im);

%%----------------------genera superpixel--------------------%%
    imname=[imname(1:end-4) '.bmp'];% il software SLIC supporta solo immagini .bmp
    comm=['SLICSuperpixelSegmentation' ' ' [imgRoot imname] ' ' int2str(20) ' ' int2str(spnumber) ' ' supdir];
    [status_comm,result_comm] = system(comm); %esegue la segmentazione SLIC e sopprime l'output    
    spname=[supdir imname(1:end-4) '.dat'];
    superpixels=ReadDAT([m,n],spname); % matrice di superpixel etichettati
    spnum=max(superpixels(:)); % l'effettivo numero di superpixel

%%----------------------creazione del grafo--------------------------%%
% calcola la funzione per ogni nodo nello spazio colore LAB.
% LAB: (L,a,b) -> Luminosit�, dimensioni colore-apparente

 %ottieni nodi
    input_vals=reshape(input_im, m*n, k); %compatta immagine in dimensione m*n ogni componente lunga k
    rgb_vals=zeros(spnum,1,3);
    inds=cell(spnum,1); % crea matrice di celle spnum x 1
    for i=1:spnum % per ogni superpixel
        inds{i}=find(superpixels==i); %trova ed assegna l'indice alla matrice di celle
        rgb_vals(i,1,:)=mean(input_vals(inds{i},:),1); %assegna come RGB la media degli indici trovati
    end  
    lab_vals = colorspace('Lab<-', rgb_vals); % converte i valori RGB in LAB 
    seg_vals=reshape(lab_vals,spnum,3);% compatta i nuovi valori LAB come valori segmentati in dimensione come superpixel

 % ottieni archi
    adjloop=AdjcProcloop(superpixels,spnum); % calcola la matrice di adiacenza
    edges=[];
    for i=1:spnum % per ogni superpixel
        indext=[];
        ind=find(adjloop(i,:)==1); % trova i nodi adiacenti
        for j=1:length(ind) % per ogni nodo adiacente
            indj=find(adjloop(ind(j),:)==1); %trova l'adiacente specifico
            indext=[indext,indj]; % accoda il nuovo indice trovato
        end
        indext=[indext,ind]; % accoda tutti i rimanenti nodi
        indext=indext((indext>i));
        indext=unique(indext); % elimina i nodi duplicati
        if(~isempty(indext)) % se esistono nodi adiacenti
            ed=ones(length(indext),2); % crea un array di tutti uno di dimensione nodi adiacenti
            ed(:,2)=i*ed(:,2); % la seconda colonna contiene l'indice del superpixel
            ed(:,1)=indext; % la prima colonna contiene gli indici adiacenti
            edges=[edges;ed]; % accoda gli archi calcolati
        end
    end

% calcola la matrice di affinit�
    weights = makeweights(edges,seg_vals,theta); % calcola i pesi degli archi
    W = adjacency(edges,weights,spnum); % calcola la matrice di adiacenza pesata con archi e nodi superpixel

% matrice di affinit� ottima
% f = (D - a*W)^-1 * y
    dd = sum(W); D = sparse(1:spnum,1:spnum,dd); clear dd; % D
    optAff =(D-alpha*W)\eye(spnum); % matrice di affinit� ottima -> A = (D - a*W)^-1
    mz=diag(ones(spnum,1)); % ottiene gli elementi della diagonale
    mz=~mz; % y
    optAff=optAff.*mz; % f = A * y

%%-----------------------------fase 1--------------------------%%
% calcola il valore della salienza per ogni superpixel con le query di
% contorno

% TOP: contorno superiore come query
    Yt=zeros(spnum,1);
    bst=unique(superpixels(1,1:n));%prende indice contorno superiore
    Yt(bst)=1;
    bsalt=optAff*Yt; % applica la formula f = A * y
    bsalt=(bsalt-min(bsalt(:)))/(max(bsalt(:))-min(bsalt(:)));
    bsalt=1-bsalt; % background saliency top

% DOWN: contorno inferiore come query
    Yd=zeros(spnum,1);
    bsd=unique(superpixels(m,1:n)); %prende indice contorno inferiore
    Yd(bsd)=1;
    bsald=optAff*Yd; % applica la formula f = A * y
    bsald=(bsald-min(bsald(:)))/(max(bsald(:))-min(bsald(:)));
    bsald=1-bsald; % background saliency down

% RIGHT: contorno destro come query
    Yr=zeros(spnum,1);
    bsr=unique(superpixels(1:m,1));%prende indice contorno destro
    Yr(bsr)=1;
    bsalr=optAff*Yr; % applica la formula f = A * y
    bsalr=(bsalr-min(bsalr(:)))/(max(bsalr(:))-min(bsalr(:)));
    bsalr=1-bsalr; % background saliency right

% LEFT: contorno sinistro come query
    Yl=zeros(spnum,1);
    bsl=unique(superpixels(1:m,n));%prende indice sinistro 
    Yl(bsl)=1;
    bsall=optAff*Yl; % applica la formula f = A * y
    bsall=(bsall-min(bsall(:)))/(max(bsall(:))-min(bsall(:)));
    bsall=1-bsall; % background saliency left

% combina le 4 mappe rilevate
    bsalc=(bsalt.*bsald.*bsall.*bsalr);
    bsalc=(bsalc-min(bsalc(:)))/(max(bsalc(:))-min(bsalc(:))); %mappa di salienza combinata

% assegna il valore di salienza ad ogni pixel     
     tmapstage1=zeros(m,n);
     for i=1:spnum
        tmapstage1(inds{i})=bsalc(i); %mappa temporanea
     end
     tmapstage1=(tmapstage1-min(tmapstage1(:)))/(max(tmapstage1(:))-min(tmapstage1(:)));

     mapstage1=zeros(w(1),w(2));
     mapstage1(w(3):w(4),w(5):w(6))=tmapstage1; %assegna i valori nella mappa di salienza della fase 1
     mapstage1=uint8(mapstage1*255);  % converte i valori della mappa a 8bit intero senza segno

     outname=[saldir imname(1:end-4) '_stage1' '.png']; % nomina il file
     imwrite(mapstage1,outname); %salva l'immagine

%%----------------------fase 2-------------------------%%
% mappa binaria con soglia adattiva (ad esempio media della mappa di salienza) 
    if(threshold == 'm') % se il parametro passato � 'm'
        th=mean(bsalc); % media della mappa di salienza
    else 
        th = threshold; %altrimenti considera il parametro come soglia (per i test)
    end
    bsalc(bsalc<th)=0; % se minore di soglia allora = 0
    bsalc(bsalc>=th)=1; % se maggiore uguale alla soglia allora = 1

% calcola i valori di salienza per ogni superpixel
    fsal=optAff*bsalc; % f = A * y

% assegna il valore di salienza ad ogni pixel
    tmapstage2=zeros(m,n);
    for i=1:spnum
        tmapstage2(inds{i})=fsal(i); %mappa temporanea
    end
    tmapstage2=(tmapstage2-min(tmapstage2(:)))/(max(tmapstage2(:))-min(tmapstage2(:)));

    mapstage2=zeros(w(1),w(2));
    mapstage2(w(3):w(4),w(5):w(6))=tmapstage2;%assegna i valori nella mappa di salienza della fase 2
    mapstage2=uint8(mapstage2*255); % converte i valori della mappa a 8bit intero senza segno
    outname=[saldir imname(1:end-4) '_stage2' '.png']; % nomina il file
    imwrite(mapstage2,outname);%salva l'immagine
end