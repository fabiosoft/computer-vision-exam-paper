% Test for paper "Rilevazione salienza attraverso classificazione multipla basata su grafo" 
% by Fabio Nisci
% Napoli, Parthenope University , 2014.

% All rights reserved.
% No part of this work may be reproduced in any form without prior permission of the author.
% This work is for computer vision exam.
% NOT for commercial use.

clear all %pulizia delle variabili
imgRoot='./test/'; % cartella che contiene le immagini di test
% raccolgo immagini dalla cartella
imnames=dir([imgRoot '*' '_gt.jpg']);

soglia_debug = [];
precision = []; recall = []; f_measure = [];

% esamino le immagini 1xGT 1xRGB
imgsToExaminate=floor(length(imnames));
for ii=1:1 %imgsToExaminate %ciclo le immagini da esaminare
    disp(['RUN ' int2str(ii) ' di ' int2str(imgsToExaminate)])
    %pulizia del nome da _gt 
    nomefile=imnames(ii).name;
    nomefile = strrep(nomefile,'_gt','');
    
    [impath,imname,imestensione]=fileparts(nomefile); % estrae dal file path, nome ed estensione
    impath = [imgRoot imname imestensione];
    imnamegt=[imname '_gt' imestensione]; %identifica il ground-truth
    imnamergb=[imname imestensione]; %identifica l'immagine a colori
%     disp(['rgb: ' imnamergb]);
%     disp(['gt: ' imnamegt]);
    
    time_gt = tic;
    % calcolo mappa di salienza del ground-truth con soglia media
    [sal_gt,soglia_gt]=saliency(imnamegt,'m');
    toc(time_gt); % tempo di calcolo ground-truth
    
    %vario soglia per avere i vari valori.
    time_soglie = tic;
    for tt=1:52 
        %reset degli indicatori
        tp=0; fp=0; fn=0;
        %img rgb con soglia variabile.
        % da 0 a 0.5 step 0.01 cos� prendo l'intero range di soglia (media = 0.2)
        soglia = (tt-1) * 0.01;
        [sal_rgb,soglia_rgb]=saliency(imnamergb,soglia);  % calcolo mappa di salienza del ground-truth con soglia adattiva

        %i size possono essere diversi quindi prendo il minore per combaciare entrambi
        pixel_da_confrontare=min(size(sal_gt),size(sal_rgb));
        righe = pixel_da_confrontare(1);
        colonne = pixel_da_confrontare(2);
        % confronta 1 a 1 i pixel per stabilire la precisione della salienza
        % scorro righe e colonne per cercare i pixel differenti
        for rr=1:righe
            for cc=1:colonne
                % true positive
                if(sal_gt(rr,cc) > 1 && sal_rgb(rr,cc) > 1)
                    tp = tp+1;
                end
                % false positive
                if(sal_gt(rr,cc) == 0 && sal_rgb(rr,cc) > 1)
                    fp = fp+1;
                end
                % false negative
                if(sal_gt(rr,cc) > 1 && sal_rgb(rr,cc) == 0)
                    fn = fn+1;
                end
            end
        end

% DEBUG i risultati ottenuti
%          disp('Risultati')
%          disp(['tp: ' int2str(tp)])
%          disp(['fp: ' int2str(fp)])
%          disp(['fn: ' int2str(fn)])
%          disp('')
        
        % soglia_debug(end+1) = soglia_rgb; %DEBUG della soglia
        precision(end + 1) = (tp)/(tp+fp); % calcola la precisione
        recall(end + 1) = (tp)/(tp+fn); % calcola il rechiamo
        beta = 0.3; %beta < 1 per enfatizzare la precisione
        f_measure(end + 1) = ((1+(beta)^2)*precision(tt)*recall(tt))/((beta)^2*precision(tt)+recall(tt)); %calcola misura F (media armonica)
        
        % conversione da NaN a Zero.
        precision(find(isnan(precision))) = 0;
        recall(find(isnan(recall))) = 0;
        f_measure(find(isnan(f_measure))) = 0;
    end
    toc(time_soglie); %tempo di calcolo di tutte le soglie
    
    % **** OUTPUT ****
    disp('')
    disp(nomefile)
    disp(['S = ' num2str(soglia_debug,'%.2f\t') ])
    disp(['P = ' num2str(precision,'%.2f\t') ])
    disp(['R = ' num2str(recall,'%.2f\t') ])
    disp(['F = ' num2str(f_measure,'%.2f\t') ])
    disp('')
    % **** OUTPUT ****
    
    %trasponi i vettori di dati ottenuti per migliorare per il grafico
    %finale PR
    precision = precision';
    recall = recall';
    f_measure = f_measure';
    
end