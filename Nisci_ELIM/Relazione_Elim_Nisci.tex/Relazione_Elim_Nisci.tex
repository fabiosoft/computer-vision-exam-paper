\input{mmd-memoir-header}
\usepackage{amssymb}
\def\mytitle{Rilevazione salienza attraverso classificazione multipla basata su grafo}
\def\myauthor{Fabio Nisci}

\def\bibliocommand{\bibliography{../res/Bibliography}}
\def\latexmode{memoir}
\input{mmd-memoir-begin-doc}
\chapter{Introduzione}
\label{introduzione}

\section{Problema}
\label{problema}

Predire dove gli occhi umani guardano quando osservano un'immagine ha guadagnato una popolarità significante negli ultimi anni. In questo scritto, sarà presentato un nuovo metodo per l'apprendimento bottom-up della salienza visiva, riferito alla parte dell'immagine che possiede maggiore importanza, la quale ben si adatta a individuare gli oggetti di interesse in scene complesse.

Viene rappresentata l'immagine come un grafo ciclico con superpixels come nodi. Questi nodi sono classificati in base alla somiglianza con le query di sfondo e primo piano, sulla base di matrici di affinità.

Il compito della \emph{saliency detection} è di identificare il soggetto come la parte più importante nella scena.

Predire le parti salienti di una immagine ha recentemente guadagnato un enorme interesse nella \emph{computer vision}. Ciò è particolarmente importante poiché una previsione consente di filtrare le informazioni irrilevanti all'interno di un'immagine e di concentrarsi sui bit che sono davvero importanti. A tal riguardo, la salienza visiva è stata utilizzata per migliorare le prestazioni di diversi compiti di visione artificiale, tra cui l'image retargeting.~\citep{image_retargeting}

La maggior parte dei lavori sulla salienza visiva si concentrano nel predire dove si fisserà l'occhio. Tuttavia c'è un crescente numero di studi che mirano al compito alternativo dell'individuare gli oggetti salienti.

Queste recenti metodologie possono essere divise in due gruppi: bottom-up e top-down, in base a come definiscono gli oggetti salienti. 

I modelli bottom-up per lo più si basano su segnali di basso livello come l'intensità, il colore, la consistenza, ecc e cercano di localizzare gli oggetti che presentano caratteristiche distinte da ciò che li circonda. Dall'altra parte gli approcci top-down sono \emph{orientati ai compiti} e cercano un oggetto di una specifica categoria. Quindi impiegano le caratteristiche fisiche dell'oggetto di interesse.

Le regioni salienti sono di solito relativamente compatte (in termini di distribuzione spaziale) ed apparentemente omogenee, mentre regioni di sfondo sono l'opposto.

\section{Motivazione}
\label{motivazione}

Notiamo che i modelli di salienza sono stati sviluppati per predire l'occhio e la rilevazione di oggetti salienti.

Il primo si concentra sull'identificazione di alcune posizioni di \emph{fissaggio umane} su immagini naturali, il quale è importante per comprendere l'attenzione umana. Invece il compito di quest'ultimo è di rilevare esattamente dove l'oggetto saliente dovrebbe essere, il che è utile per vari compiti di visione ad alto livello. In questo lavoro, ci concentriamo sul rilevamento di oggetti saliente in maniera bottom-up.

\section{Contributo}
\label{contributo}

Osserviamo che lo sfondo spesso presenta una apparente connessione locale o globale con ciascuno dei quattro confini dell'immagine, ed il primo piano presenta una coerenza e consistenza. In questo lavoro sfruttiamo queste caratteristiche per calcolare i pixel salienti basati sulla classificazione dei superpixels. Per ogni immagine costruiamo un grafo ciclico dove ogni nodo è un superpixel. Modelliamo la rilevanza della salienza come un problema di molteplice classificazione e proponiamo uno schema a due fasi per l'etichettatura del grafo. In figura $\ref{fig-modello}$ vengono mostrati i passaggi principali dell'algoritmo proposto.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.50\textwidth]{../res/pics/proposed_model.png}
    \caption{Schema del modello proposto.}
    \label{fig-modello}
\end{figure}

Nella \textbf{prima fase}, sfruttiamo la priorità dei confini usando i nodi di ciascun lato dell'immagine etichettati come \emph{query di sfondo}. Per ogni risultato etichettato, si calcola la salienza dei nodi in base alla loro rilevanza (es. il punteggio) a queste query come etichette di sfondo. Le quattro mappe etichettate sono poi integrate per generare una mappa di salienza. 

Nella \textbf{seconda fase} applichiamo la segmentazione binaria sulla mappa di salienza risultante dalla prima fase, prendiamo i nodi etichettati di primo piano come query di salienza. La salienza di ciascun nodo è calcolata in base alla sua rilevanza per le query di primo piano nella mappa finale.

Per catturare appieno le informazioni della struttura del grafo e raggruppare i segnali etichettati, usiamo molteplici tecniche di classificazione per studiare una funzione di classificazione, essenziale per studiare una matrice di affinità ottimale.

\section{Contenuto}
\label{contenuto}

Nella presente relazione viene descritto e testato un algoritmo, sviluppato presso \emph{Dalian University of Technology}, \emph{OMRON Corporation} e \emph{University of California at Merced}.

Il problema di classificazione basato su grafo è descritto come segue: dato un nodo come query, i nodi rimanenti sono classificati in base alla loro rilevanza rispetto al nodo di partenza. L'obiettivo è lo sviluppo di una funzione di classificazione, la quale definisce la rilevanza tra nodi e query non etichettate.

Nella sezione ``discussioni'' vengono forniti alcuni consigli per poter utilizzare al meglio il software, per evitare di incorrere nelle medesime difficoltà riscontrate in fase di primo utilizzo del software.

Dopo aver collocato il problema appena descritto nella letteratura corrente, nelle sezioni successive viene descritta nel dettaglio la strategia proposta

La relazione si conclude con la sezione denominata Risultati che descrive una serie di test effettuati sull'algoritmo e le conclusioni sull'argomento.

\pagebreak 

\chapter{Letteratura}
\label{letteratura}

\section{Tecniche disponibili}
\label{tecnichedisponibili}

La salienza è definita da quelle caratteristiche fisiche di un oggetto che fanno sì che questo possa emergere rispetto al contesto in cui si trova. In generale, un oggetto è saliente se ha una forma, un colore, un orientamento, e dimensioni diverse rispetto al contesto. Tale diversità catturerà l'attenzione dell'osservatore. La rilevanza, per contro, è ciò che attribuisce importanza ad un oggetto non per le sue caratteristiche fisiche, ma per i suoi significati. Salienza e rilevanza concorrono nel catturare e guidare la nostra attenzione (visual attention), e quindi il nostro sguardo, quando sfogliamo un giornale, guardiamo un film, navighiamo in internet ma anche nel mondo fisico che ci circonda.

Questa ricerca prende in esame il ruolo della salienza percettiva degli elementi e della loro rilevanza rispetto al senso globale della scena.

Algoritmi di rilevamento degli oggetti salienti di solito generano delle caselle di delimitazione per sfondo e primo piano, o mappe di salienza che indicano la probabilità di salienza di ogni pixel. Liu et al. ~\citep{liu_et_al} propone un modello di stima di salienza binario attraverso la formazione di un campo casuale condizionale per combinare una serie di nuove funzionalità. Wang et al. ~\citep{wang_et_al} analizza molteplici segnali in un contesto di minimizzazione d'energia ed usa un modello di salienza basato su grafi per rilevare gli oggetti salienti.

\textbf{Individuazione bottom-up di oggetti salienti.} L'obiettivo dell'individuazione bottom-up di oggetti salienti è quello di individuare gli oggetti importanti in un'immagine che attirino l'attenzione sotto condizioni visive normali. Differiscono principalmente tra loro per il modo in cui definiscono e stimino la salienza per mezzo di misure di contrasto differenti. Esempi recenti includono: Perazzi et al. ~\citep{perazzi_et_al} segmenta un'immagine in superpixels e stima la salienza su di loro considerando due misure di contrasto che rispettivamente dipendono dalla loro unicità e distribuzione cromatica. Margolin et al. propone un contesto combinato che coinvolge le strategie di patch e superpixel, le quali sono rispettivamente basate su modello (pattern) e rarità di colore. Yang et al. ~\citep{zhou2003scholkopf} individua le regioni salienti risolvendo un problema di classificazione basato su grafi, il quale è definito attraverso alcuni raggruppamenti locali e priorità di sfondo. Questi metodi bottom-up mostrano buone prestazioni su esistenti dataset di immagini. Tuttavia le priorità da cui dipendono sono di natura generica e quindi generano risultati \textbf{insoddisfacenti} come illustrato per il compito di \emph{trova la bicicletta}.

\textbf{Modelli oggettivi generici.} Una linea di ricerca nel riconoscimento di oggetti che condivide molte somiglianze con il metodo bottom-up della ricerca di oggetti salienti si concentra su localizzare tutti gli oggetti in un'immagine in maniera indipendente dalla classe. Questi modelli \emph{oggettivi} integrano molteplici spunti, tutti misurano le caratteristiche distintive generiche di oggetti come avere un contorno chiuso; essendo apparentemente diversi dall'ambiente che li circonda, generare un piccolo numero di proposte di oggetti in termini di finestre o segmenti senza impiegare nessuna categoria specifica di conoscenza. Tuttavia, il fatto che si basino su metodi bottom-up gli rende difficile localizzare uno specifico oggetto di interesse.

\textbf{Rilevazione oggetti salienti top-down.} Nella letteratura di \emph{computer vision} ci sono numerosi approcci ai modelli computazioni di salienza bottom-up. Non molto lavoro si è però concentrato sulla stima dei valori di salienza per metodi top-down. Un prematuro modello per le attività di ricerca sulle immagini del mondo reale è il modello \emph{guida contestuale} di Torralba et al. ~\citep{torralba_et_al} il quale deriva dal contesto Bayesiano e combina bassi livelli di salienza e contesto della scena. Ultimamente ci sono alcuni studi che calcolano le mappe del peso di immagini per alcuni compiti come il riconoscimento degli oggetti, segmentazione degli oggetti e classificazione di immagine, i quali sono intrinsecamente connessi alla salienza top-down.

\section{Vantaggi e Svantaggi}
\label{vantaggiesvantaggi}

I metodi menzionati misurano la salienza misurando il contorno-centrale locale e la rarità delle funzioni su tutta l'immagine. In contrasto Gopalakrishnan et al. ~\citep{gopalakrishnan_et_al} formula un problema di rilevazione degli oggetti e etichettatura su grafo. I semi della salienza e molti semi dello sfondo sono identificati dal comportamenti dei cammini casuali su un grafo completo e un grafo k-regolare. Poi, una tecnica semi-supervisionata viene utilizzata per dedurre le etichette binarie di nodi senza etichetta. Recentemente, un metodo che sfrutta le priorità dello sfondo è stato proposto per la deduzione della salienza. La principale osservazione è che la distanza tra una coppia di regioni di sfondo è minore di quella tra un oggetto saliente e una regione di sfondo. Il compito di etichettatura dei nodi (sia salienti che di sfondo) è formulato come un problema della minimizzazione dell'energia basato su questo criterio.

L'algoritmo di estrazione della salienza con molteplice classificazione proposto richiede solo semi di una classe, i quali sono inizializzati come le priorità di margine o segnali del primo piano.

Le priorità di margine sono proposte ispirandosi ai recenti lavori di fissazione visiva umana sulle immagini, i quali mostrano che gli esseri umani tendono a guardare fisso il centro delle immagini. Queste priorità sono anche usate nella segmentazione di immagini e nei problemi correlati. In contrasto, il metodo semi-supervisionato richiede entrambi i semi di sfondo e salienti, e genera una segmentazione binaria. Inoltre, è difficile determinare il numero e le posizioni dei semi salienti, come vengono generati dai cammini casuali, specialmente per scene con differenti oggetti salienti. Questa è una problematica conosciuta con il grafo etichettato dove i risultati sono sensibili ai semi selezionati. In questo lavoro, tutti i semi di sfondo e primo piano posso essere facilmente generati attraverso le priorità dello sfondo e la classificazione delle query di sfondo. Come il nostro modello incorpora i segnali estratti dall'intera immagine, l'algoritmo proposto genera confini ben definiti degli oggetti salienti ed evidenzia in modo uniforme le intere regioni salienti.

\pagebreak 

\chapter{Metodi e tecniche}
\label{metodietecniche}

\section{Metodi e tecniche}
\label{metodietecniche}

Osserviamo che lo sfondo spesso presenta una apparente connessione globale con i quattro margini ed il primo piano presenta coerenza e consistenza. In questo scritto sfruttiamo queste caratteristiche per calcolare i pixel salienti in base al punteggio dei superpixels. Per ogni immagine, costruiamo una grafo ciclico dove ogni nodo è un superpixel. Modelliamo il rilevamento saliente come un problema di molteplice classificazione e proponiamo una schema a due fasi per l'etichettatura del grafo. La figura $\ref{fig-modello}$ mostra i passi principali dell'algoritmo proposto. Nella prima fase, sfruttiamo la priorità dei margini usando nodi su ciascun lato dell'immagine come query di sfondo etichettate. Da ogni risultato etichettato, calcoliamo la salienza dei nodi in base alla loro rilevanza (ovvero al punteggio) di queste query come etichette di sfondo. Le quattro mappe etichettate vengono poi unite per generare una mappa di salienza. Nella seconda fase, applichiamo la segmentazione binaria sulla mappa di salienza risultante dalla prima fase, e usiamo i nodi etichettati di primo piano come query. La salienza di ogni nodo è calcolata in base alla sua rilevanza alla query di primo piano per la mappa finale. 

Il problema di classificazione basato su grafo è descritto come segue: dato un nodo come query, i nodi rimanenti sono classificati in base alla loro rilevanza rispetto al nodo di partenza. L'obiettivo è lo sviluppo di una funzione di classificazione, la quale definisce la rilevanza tra nodi e query non etichettate.

\section{Classificazione Intrinseca}
\label{classificazioneintrinseca}

Viene proposto un metodo di classificazione che sfrutta la struttura intrinseca dei dati (come un'immagine) per l'etichettatura del grafo.
Dato un dataset $X = \left\{x_1,\space\ldots,x_l,x_{l+1}\space\ldots,x_n\right\} \in \mathbb{R}^{m\space\times\space n}$, alcuni datapoint sono query etichettate e il resto ha bisogno di essere classificato in base alle loro pertinenze con le query. Sia $f:X\rightarrowtail\mathbb{R}^n$ una funziona di classificazione che assegna un valore di punteggio $f_i$ ad ogni punto $x_i$, e $f$ può essere vista come un vettore $f = \left[f_1,\ldots,f_n\right]^T$. Sia $y = \left[y_1,y_2,\ldots,y_n\right]^T$ denota un vettore di direzione, nel quale $y_i = 1$ se $x_i$ è una query, e $y_i = 0$ altrimenti. Successivamente definiamo un grafo $G = (V,E)$ sul dataset, dove i nodi $V$ sono il dataset $X$ e gli archi $E$ sono pesati da una matrice di affinità $W = \left[w_{ij}\right]n\times n$. Dato $G$ la matrice gradiente è $D = diag\left\{d_{11},\ldots,d_{nn}\right\}$ dove $d_{ii}=\sum_jw_{ij}$. Similmente agli algoritmi di PageRank e clustering spettrale la classificazione ottimale delle query è calcolata risolvendo il seguente problema di ottimizzazione: 

\begin{equation} \label{eq:problemaottimizzazione}
f^*=\arg\min_{f}\frac{1}{2}\left(\sum\limits_{i,j=1}^{n}w_{ij}  \| \frac{f_i}{\sqrt{d_{ii}}} - \frac{f_i}{\sqrt{d_{ii}}} \|^2+\mu\sum\limits_{i=1}^{n}\|f_i-y_i\|^2 \right)
\end{equation}


dove il parametro $\mu$ controlla il bilanciamento del \emph{vincolo scorrevolezza} (primo termine) e il vincolo di \emph{fitting} (il secondo termine).

L'algoritmo di classificazione è derivato dal lavoro sull'insegnamento semi supervisionato per la classificazione. Essenzialmente la molteplice classificazione può essere vista come un problema di classificazione di una sola classe, dove solo esempi positivi o negativi sono richiesti. Siamo in grado di ottenere un'altra funzione di rango utilizzando la matrice di Laplace non normalizzata.
\begin{equation} \label{eq:matricelaplace}
f^*=\left(D-\alpha W\right)^{-1}y
\end{equation}


Questa equazione $\ref{eq:matricelaplace}$ negli esperimenti raggiunge prestazioni migliori. Quindi, la adottiamo in questo lavoro.

\section{Misura della salienza}
\label{misuradellasalienza}

Data un'immagine in input rappresentata come un grafo ed alcuni nodi salienti, la salienza di ogni nodo è definita come il suo punteggio di classificazione calcolato dall'equazione $\ref{eq:matricelaplace}$ la quale viene riscritta come $f^*=Ay$ per semplificare l'analisi. La matrice A può essere considerata come una matrice di affinità ottimale, come abbiamo visto, la quale è uguale a $\left(D-\alpha W\right)^{-1}$.

Poiché $y$ è un vettore binario, $f^*\left(i\right)$ può anche essere vista come la somma delle rilevanze del $i$-esimo nodo di tutte le query.

Nei problemi di classificazione convenzionali, le query sono etichettate manualmente con il \emph{ground-truth}. Indicando con \emph{GT} (Ground Truth) una immagine contenente il background \emph{vero}.

Tuttavia, dato che le query per la determinazione della salienza sono selezionate dall'algoritmo proposto, alcune di loro potrebbero essere incorrette. Per questo motivo abbiamo bisogno di calcolare il grado di confidenza (ovvero il valore di salienza) per ciascuna query, il quale è definito come il suo punteggio dalle altre query (eccetto se stessa). A tal fine, abbiamo impostato gli elementi diagonali di A a 0 quando si calcola il punteggio.

Se calcolassimo il punteggio di salienza per ciascuna query senza impostare gli elementi della diagonale di A a 0, il suo punteggio in $f^*$ conterrebbe la rilevanza di se stesso, che non ha senso e spesso anormalmente grande da indebolire fortemente i contributi delle altre query al calcolo del punteggio.

\section{Costruzione del grafo}
\label{costruzionedelgrafo}

$\label{section:graphconstruction}$
Costruiamo un grafo a livello singolo $G = \left(V,E\right)$ come in figura $\ref{fig-graph_model.png}$, dove $V$ è un insieme di nodi e $E$ un insieme di archi. In questo lavoro ogni nodo è un superpixel generato dall'algoritmo di \emph{SLIC}. ~\citep{slic}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.50\textwidth]{../res/pics/graph_model.png}
    \caption{Il nostro modello di grafo. La linea rossa lungo i quattro lati indica che tutti i nodi ai margini sono collegati tra di loro.}
    \label{fig-graph_model.png}
\end{figure}
Innanzitutto, ogni nodo non è solo connesso ai nodi vicini, ma anche connesso ad i nodi che condividono confini comuni con il suo nodo adiacente. Estendendo il campo di connessione in figura $\ref{fig-graph_model.png}$ del nodo con lo stesso grado di k, abbiamo effettivamente utilizzato i punti locali. In secondo luogo, abbiamo rafforzato l'idea che i nodi sui quattro lati dell'immagine siano connessi, ovvero ogni paio di nodi di margine sono da considerarsi adiacenti. Perciò, indichiamo questo grafo come ciclico. Questo grafo ciclico migliora significativamente le prestazioni del metodo proposto in quanto tende a ridurre la distanza più breve delle linee tracciate per unire due superpixels simili, migliorando così i risultati di classificazione.

Notiamo che questi vincoli funzionano bene quando gli oggetti salienti appaiono vicino ai margini dell'immagine o alcune delle regioni di sfondo non sono le stesse.

Con questi vincoli sui bordi, è chiaro che il grafo costruito è sparso.

\section{Schema a due fasi}
\label{schemaaduefasi}

In questa sezione, vediamo in dettaglio lo schema a due fasi proposto per la rilevazione della salienza bottom-up usando query di classificazione per sfondo e primo piano

\section{Classificazione con query di sfondo}
\label{classificazioneconquerydisfondo}

Basandoci sulle teorie sull'attenzione dei precedenti lavori per la salienza visiva, abbiamo usato nodi sui margini dell'immagine come semi per lo sfondo, ovvero, i dati etichettati (query campione) per la classificazione di tutte le regioni. In maniera specifica, abbiamo costruito quattro mappe di salienza usando le priorità dei margini per poi integrarle tutte nella mappa finale, la quale si riferisce all'approccio separazione\slash combinazione (SC).

Prendendo il margine superiore come esempio, abbiamo usato i nodi su questo lato come query e gli altri nodi come dati non etichettati. Così, è formato il vettore $y$, e tutti i nodi sono classificati in base all'equazione $\ref{eq:matricelaplace}$ in $f^*$, il quale è un vettore di dimensione N (N è il numero totale di nodi nel grafo). Ciascun elemento in questo vettore indica la rilevanza di un nodo alle query di sfondo, ed il suo complementare è la misura della salienza. Abbiamo normalizzato questo vettore nell'intervallo 0 e 1, e la mappa di salienza usando la priorità del margine superiore.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.50\textwidth]{../res/pics/saliency_maps_example.png}
    \caption{Mappe di salienza usando query differenti. Da sinistra a destra: immagine in input, risultato con i nodi di margine, risultato dell'integrazione delle mappe, risultato.}
    \label{fig-saliency-ex}
\end{figure}

Ci sono due ragioni per usare l'approccio SC per generare le mappe di salienza. Primo, i superpixel su lati diversi sono spesso dissimili. Se simultaneamente usassimo tutti i superpixels dei margini come query (indicando questi superpixel come simili), i risultati etichettati sono spesso meno ottimi dato che questi nodi non sono compattabili come in figura $\ref{fig-saliency-ex}$. Notiamo che la distanza minima tra i punti usata nella sezione $\ref{section:graphconstruction}$ può essere considerata ``debole'' visto che solo pochi superpixels vengono coinvolti nell'etichettatura, ovvero, solo i superpixel con bassa distanza colore dai lati sono considerati simili considerando il caso che tutti i superpixels possano essere considerati come etichettati (cioè, tutti i nodi dei lati sono considerati simili). Secondo, riduce gli effetti di query imprecise, cioè, i nodi salienti \emph{ground-truth} sono inavvertitamente selezionati come query di sfondo. Dati gli imprecisi risultati di etichettatura, i pixel con oggetti salienti hanno bassi valori di salienza. Tuttavia, come gli oggetti sono spesso ``cose'' compatte (come una persona o un auto) in maniera opposta la ``roba'' non compatta (come un prato o il cielo) e dunque raramente occupano tre o tutti i lati dell'immagine, l'approccio proposto SC garantisce che almeno due mappe di salienza siano valide. Integrando le quattro mappe, alcune parte salienti di un oggetto possono essere identificate (anche se l'intero oggetto non è evidenziato in modo uniforme), fornendo indicazioni sufficienti per la seconda fase di rilevamento.

\section{Classificazione con query di primo piano}
\label{classificazioneconquerydiprimopiano}

La mappa di salienza della prima fase è segmentata in modo binario (ovvero il primo piano saliente e lo sfondo) usando una soglia adattava, la quale facilita la selezione dei nodi del primo piano come query.

Ci aspettiamo che le query selezionate ricoprano l'oggetto saliente il più possibile. Cosicché la soglia sia impostata come media di salienza sull'intera mappa di salienza.

Una volta che le query salienti vengono fornite, è formato un vettore $y$ per calcolare il vettore di punteggio $f^*$ usando $\ref{eq:matricelaplace}$.

Notiamo che ci sono casi in cui i nodi potrebbero essere non correttamente selezionati come query di primo piano in questa fase. Nonostante alcune etichettature imprecise, gli oggetti salienti possono essere ben rilevati dall'algoritmo proposto.

Ciò si \textbf{spiega} nel modo seguente. Le regioni salienti sono solitamente relativamente compatte (in termini di distribuzione spaziale) e hanno un aspetto omogeneo, mentre le regioni di sfondo sono l'opposto. In altre parole, la rilevazione \emph{intra-oggetto} è statisticamente più grande si quella \emph{oggetto-sfondo} e rilevanza \emph{intra-sfondo}, la quale può essere dedotta da una matrice di affinità $A$. Per mostrare questo fenomeno, calcoliamo la media \emph{intra-oggetto}, \emph{intra sfondo} e \emph{oggetto-sfondo}.

\section{Algoritmo}
\label{algoritmo}

La fase principale dell'algoritmo di rilevazione degli oggetti viene riassunta in questo algoritmo.

\textbf{Input:} un'immagine ed i parametri richiesti

\begin{enumerate}
\item Segmenta l'immagine di input in superpixels, costruisce un grafo $G$ con i superpixels come nodi, e calcola la sua matrice $D$ di grado pesata $W$.

\item Calcola $\left(D-\alpha W\right)^{-1}$ ed imposta gli elementi della diagonale a 0.

\item Forma il vettore $y$ con nodi su ogni lato dell'immagine come query, e calcola le loro mappe corrispondenti ai lati con l'equazione $\ref{eq:matricelaplace}$. Dopodiché, calcola la mappa di salienza $S_{bq}$.

\item Segmenta $S_{bq}$ per formare le query di primo piano salienti ed il vettore di direzione $y$. Calcola la mappa di salienza finale.

\end{enumerate}

\textbf{Output:} una mappa di salienza $S_{fq}$ che rappresenta il valore della salienza di ciascun pixel.

\pagebreak 

\chapter{Risultati}
\label{risultati}

\section{Panoramica}
\label{panoramica}

Valutiamo il metodo proposto su tre set di dati. Il primo è il dataset \textbf{MSRA} il quale contiene 5000 immagini con il \emph{ground-truth} della regione saliente marcato da riquadri. Il secondo è il dataset \textbf{MSRA--1000}, un sottoinsieme del dataset MSRA, che contiene 1000 immagini con precise maschere etichettate a mano di oggetti salienti. L'ultimo è il dataset \textbf{DUT-OMRON}, il quale contiene 5172 immagini accuratamente etichettate da cinque utenti. Le immagini sorgenti e le descrizioni dettagliate posso essere trovate !!! qui http:/\slash ice.dlut.edu.cn\slash lu\slash DUT-OMRON\slash Homepage.htm. Abbiamo confrontato il nostro metodo con quattordici algoritmi di rilevamento della salienza in commercio.

\textbf{Setup sperimentale:} impostiamo il numero dei nodi superpixel a $N=200$ in tutti gli esperimenti. Ci sono due parametri nell'algoritmo: il peso degli archi $\sigma$ e il bilanciamento dei pesi $\alpha$ in $\ref{eq:matricelaplace}$. Il parametro peso $\sigma$ controlla la forza peso tra una coppia di nodi ed il parametro $\alpha$ bilancia i vincoli di \emph{fitting} nella regolarizzazione dell'algoritmo di punteggio. Questi due parametri sono scelti empiricamente, $\sigma^2=0.1$ e $\alpha = 0.99$, per tutti gli esperimenti.

Il valore di precisione corrisponde al rapporto di pixel salienti assegnati a tutti i pixel delle regioni estratte, mentre il valore di richiamo viene definito come la percentuale di pixel salienti identificati in relazione al numero \emph{ground-truth}. Simili ai lavori precedenti, le curve di richiamo sono ottenute dalla binarizzazione della mappa di salienza con soglie nell'intervallo 0 e 255.

\section{MSRA--1000}
\label{msra-1000}

Per prima cosa esaminiamo le opzioni di progettazione dell'algoritmo proposto nei dettagli.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{../res/pics/precision_curves.png}
    \caption{Curve di richiamo su dataset MSRA-1000}
    \label{fig-prec_curves}
\end{figure}
I risultati classificati usando la matrice normalizzata e non normalizzata $\ref{eq:matricelaplace}$ di Laplace. La figura $\ref{fig-prec_curves}$(a) mostra che i risultati della classificazione con la matrice non normalizzata di Laplace sono migliori, e saranno usati in tutti gli esperimenti. Successivamente, dimostriamo il merito dello schema di costruzione del grafo proposto.

Calcoliamo quattro curve di richiamo per quattro casi di connessioni sul grafo:

\begin{itemize}
\item vincolo ciclico senza estendere il campo del nodo con il grafo k-regolare,

\item senza vincolo ciclico e con grafo k-regolare,

\item senza il vincolo ciclico e senza grafo k-regolare

\item vincolo ciclico con grafo k-regolare.

\end{itemize}

La figura $\ref{fig-prec_curves}$(b) mostra che l'uso di un vincolo ciclico e un grafo regolare funziona meglio. L'effetto dell'approccio SC nella prima fase viene tenuto in considerazione. La figura $\ref{fig-prec_curves}$(c) mostra che il nostro approccio, usando l'integrazione delle mappe di salienza da margini differenti, funziona meglio nella prima fase. Confrontiamo ulteriormente le prestazioni per ogni fase dell'algoritmo proposto. La figura $\ref{fig-prec_curves}$(d) dimostra che la seconda fase, usando le query di primo piano, migliorano ulteriormente le prestazioni della prima fase con le query di sfondo.

\section{MSRA}
\label{msra}

Valutiamo inoltre l'algoritmo proposto sul dataset MSRA in cui le immagini vengono etichettate da diversi utenti. Per calcolare i valori di precisione e di richiamo, per prima cosa misuriamo un rettangolo per la mappa binaria di salienza per successivamente utilizzare il riquadro di output per la valutazione. In maniera simile agli esperimenti sul database MSRA--1000, rendiamo binaria le mappe di salienza usando una soglia del doppio della salienza media per calcoli precisi.

\section{DUT-OMRON}
\label{dut-omron}

Verifichiamo il modello proposto sul set di dati DUT-OMRON in cui le immagini vengono etichettate con riquadri di delimitazione da cinque utenti. Similmente agli esperimenti sul database MSRA, calcoliamo un rettangolo binario di mappa saliente e dopodiché valutiamo il nostro modello con una soglia fissa ed una soglia adattava. Il dataset proposto è più impegnativo (tutti i modelli vengono eseguiti male), e fornisce quindi più spazio per il miglioramento del lavoro futuro.

\section{Tempi di esecuzione}
\label{tempidiesecuzione}

Il tempo medio di esecuzione dei metodi attuali con migliori prestazioni, utilizzando l'implementazione \emph{Matlab} sul dataset MSRA--1000 sono presentati nella Tabella $\ref{tb:runtimes}$ basati su macchina con Macintosh CPU Intel Core i5 2.7 GHz e 12GB RAM di metà 2011. Il nostro tempo di esecuzione è molto più veloce di quello degli altri modelli. In particolare, la generazione dei superpixel dall'algoritmo SLIC spende 0.165 s (circa 64\%), e l'effettivo calcolo della salienza spende 0,091 s.
$\label{tb:runtimes}$

\begin{table}[htbp]
\begin{minipage}{\linewidth}
\setlength{\tymax}{0.5\linewidth}
\centering
\small
\begin{tabulary}{\textwidth}{@{}CCCCC@{}} \toprule
Metodi&Nostro&CB&Gof&SVO\\
\midrule
Tempo(s)&0.256&2.146&38.896&79.861\\

\bottomrule

\end{tabulary}
\end{minipage}
\end{table}


Tabella $\ref{tb:runtimes}$. Confronto di tempo medio di esecuzione (secondi per immagine).

\pagebreak 

\chapter{Conclusioni}
\label{conclusioni}

\section{Discussione}
\label{discussione}

\textbf{MATLAB} (abbreviazione di Matrix Laboratory) è un ambiente per il calcolo numerico e l'analisi statistica scritto in C che comprende anche l'omonimo linguaggio di programmazione creato dalla MathWorks. MATLAB consente di manipolare matrici, visualizzare funzioni e dati, implementare algoritmi, creare interfacce utente, e interfacciarsi con altri programmi.

\textbf{Visual Studio} è un ambiente di sviluppo integrato (IDE) sviluppato da Microsoft, che supporta attualmente diversi tipi di linguaggio, quali C, C++, Visual Basic e .Net, e che permette la realizzazione di applicazioni.

\textbf{OpenCV} è una libreria orientata alla \emph{computer vision}. Originariamente è stata sviluppata da Intel, mentre attualmente è sotto licenza open source BSD. È una libreria multipiattaforma e quindi è compatibile con molti sistemi operativi.

Al primo tentativo di compilazione vengono restituiti:

\begin{itemize}
\item 29 warning pur avendo rispettato tutte le specifiche dell'autore.

\item 1 errore per mancanza di ``msvcp100.dll'', prontamente installata anche se non fornita con il progetto.

\end{itemize}

Risolti i problemi principali, la compilazione procede correttamente, tuttavia il codice fa riferimento a funzioni con problemi conosciuti per la manipolazione dei percorsi di sistema delle immagini passate via riga di comando. Una volta risolti, si può procedere all'avvio.

Avvio che si conclude con un errore di \emph{``Assertion Failure''} da parte di OpenCV mostrato in figura $\ref{fig-opensv_error}$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.50\textwidth]{../res/pics/opencv_error.png}
    \caption{OpenCV errore: "Assertion Failure"}
    \label{fig-opensv_error}
\end{figure}

Dopo aver contattato l'autore Mr. Lihe Zhang ed appreso che si tratta di un problema già riscontrato ho applicato le modifiche suggerite:

\begin{quote}

you can try as following:
GMRsaliency.cpp::37 int* labels = new int[sz]; -$>$ int* labels; 
\end{quote}

Tuttavia l'implementazione in MATLAB è più efficiente su grandi set di dati, quindi, una volta corretto il codice da errori e bug, i test personali sono stati effettuati utilizzando quest'implementazione.

La figura $\ref{fig-griglia_test}$ mostra i risultati di alcuni test effettuati, per un totale di 5000 immagini analizzate. L'algoritmo proposto genera mappe di salienza che si avvicinano molto alla realtà.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{../res/pics/griglia_test.jpg}
    \caption{Risultati dei test.}
    \label{fig-griglia_test}
\end{figure}

\section{Conclusione}
\label{conclusione}

Abbiamo proposto un metodo bottom-up per rilevare le regioni salienti in immagini attraverso un grafo di classificazione, il quale incorpora raggruppamento locale e priorità dei margini. Adottato un approccio in due fasi con query di sfondo e primo piano per la classificazione e le generazione delle mappe di salienza. Valutato l'algoritmo su grandi dataset e dimostrato i risultati attesi con confronti su altri metodi in commercio. Inoltre, l'algoritmo proposto è \textbf{computazionalmente efficiente}.

\pagebreak 

\chapter{Setup e installazione}
\label{setupeinstallazione}

Per poter effettuare i test ed analizzare correttamente i dati vengono creati due ambienti di lavoro. La tabella $\ref{tb:sys}$ riassume i due ambienti

$\label{tb:sys}$

\begin{table}[htbp]
\begin{minipage}{\linewidth}
\setlength{\tymax}{0.5\linewidth}
\centering
\small
\begin{tabulary}{\textwidth}{@{}CCCC@{}} \toprule
&Sistema Op.&Visual Studio&OpenCV\\
\midrule
Sys 1&Windows 8&2008&2.4.3\\
Sys 2&Windows XP&2008&2.4.3\\

\bottomrule

\end{tabulary}
\end{minipage}
\end{table}


Tabella $\ref{tb:sys}$. Ambienti di lavoro.

La scelta delle versioni risiede nel fatto che, esaminando la relazione redatta, le indicazioni fornite e il codice sorgente fornito, ho scelto la versione usata per la sua creazione. Per fugare eventuali dubbi ho anche provato ad includere nei test da me effettuati versioni dei sistemi attuali, ricevendo però numerosi errori.

Per configurare il sistema si procede in questo modo:

\begin{enumerate}
\item Installazione di Microsoft Visual Studio 2008

\begin{itemize}
\item Installare Visual Studio facendo clic sul file dell'applicazione di installazione della versione desiderata, situato nella directory o sul supporto ottico di installazione.

\item Seguire le indicazioni sul video, come indicato dal sito del produttore

\item Riavviare il sistema

\end{itemize}

\item Installazione di OpenCV

\begin{itemize}
\item Estrarre il pacchetto compresso in C:\textbackslash{}opencv243

\item Copiare il contenuto della sottodirectory generata nella root opencv243

\item Aggiungere la variabile d'ambiente ``OPENCV\_BUILD'' con valore ``C:\textbackslash{}opensv243\textbackslash{}build''

\item Modificare la variabile ``path'' aggiungendo ``;\%OPENCV\_BUILD\%\textbackslash{}x86\textbackslash{}vc10\textbackslash{}bin'', per essere sicuro delle modiche siano rese effettive è consigliabile riavviare il sistema

\item Una volta aperto il progetto aggiungere una nuova property nel property manager chiamata ``opencv\_debug''

\item Dalla nuova property creata cliccare su property $>$ c\slash c++ $>$ additional include $>$ edit ed aggiungere ``\$(OPENCV\_BUILD)\textbackslash{}include''

\item In linker $>$ general $>$ edit $>$ additional library directory $>$ ``\$(OPENCV\_BUILD)\textbackslash{}x86\textbackslash{}vc10\textbackslash{}lib''

\item In linker $>$ input $>$ edit $>$ additional dependencies $>$ ``opencv\_core243d.lib opencv\_imgproc243d.lib opencv\_highgui243d.lib opencv\_ml243d.lib opencv\_video243d.lib opencv\_features2d243d.lib opencv\_calib3d243d.lib'' - Per la release la stessa configurazione senza ``d'' finale.

\end{itemize}

\item Installazione di MATLAB

\end{enumerate}

\input{mmd-memoir-footer}

\end{document}
